import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class TaskService {
nameAuthor;

  constructor(private http:HttpClient) { }

  SetAuthorTaskName(id:Number,name:String,body=null)
  { return this.http.put("http://localhost:8080/updateTaskAUthor?id="+id+"&nameAuthor="+name,body);
}
  getTasksBySprintId(id)
  {
    return this.http.get("http://localhost:8080/ListTaskBySprintId?idsprint=" + id );
  }
getAllInprogressTaskByAuthorName(username)
{
  return this.http.get("http://localhost:8080/taskinProgressbyUsername?username="+username);
}

getAllDoneTasks(username)
{
  return this.http.get("http://localhost:8080/taskinDonebyUsername?username="+username);
}
getAllToDoTasks(username)
{
  return this.http.get("http://localhost:8080/taskTodobyUsername?username="+username);
}





CountprogressionTasks(projectName): Observable<any>
{
  return this.http.get("http://localhost:8080/countStateTasks?name="+projectName);
};

  setName(name)
  {
    this.nameAuthor=name
  }
  getName()
  {return this.nameAuthor;}
}
