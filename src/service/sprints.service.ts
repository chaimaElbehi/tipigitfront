import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class sprintService{

constructor(private http:HttpClient){}
getAllSprints(nameProject:String){

  return this.http.get('http://localhost:8080/findSprintByProject?nameProject='+nameProject);
}
}
