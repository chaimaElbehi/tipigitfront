import { Observable, of } from 'rxjs';
import {delay } from 'rxjs/operators';
import { Author } from './../model/model.author';
import { Injectable } from "@angular/core";

import { Http } from "@angular/http";



@Injectable()
export class AuthorService{
  author:Author;
  isLoginAvailable:boolean;
  username : String ;
constructor(public http:Http){

}
deleteAuthor(id)
{
  return this.http.get('http://localhost:8080/author/delete?idAuthor='+id);
}
getAuthors(keyword:string,page:number,size:number){
return this.http.get('http://localhost:8080/author/searchAuthor?mc='+keyword+'&size='
+size+'&page='+page)
.map(resp=>resp.json());
}

getUsername() : Observable<String[]>
{

  let response =this.http.get("http://localhost:8080/author/getUsersByUsername")
  // ...and calling .json() on the response to return data
  .map((response) => response.json());
return response;


}
getAuthorsName() : Observable<String[]> {
  // ...using get request

  let response =this.http.get("http://localhost:8080/author/authorsName")
     // ...and calling .json() on the response to return data
     .map((response) => response.json());
 return response;
}


getAuthorInfoByusename(username)

{
  let response = this.http.get('http://localhost:8080/author/getUserbyusername?username='+username)
  // ...and calling .json() on the response to return data
  .map((response) => response.json());
return response;


}
public checkLogin(login: String) {
  // simulate http.get()
  this.getUsername().subscribe(data => {
  console.log(data.includes(login))
  console.log(data)
  if (data.includes(login)) {
  this.isLoginAvailable = true;
  } else
  this.isLoginAvailable = false;
  });
  console.log(this.isLoginAvailable);
  return of({ isLoginAvailable: this.isLoginAvailable }).pipe(delay(1000));
  }


getAllAuthors(){
    return this.http.get('http://localhost:8080/author/listAuthor')
    .map(resp=>resp.json());
}
CountAuthors(){
  return this.http.get('http://localhost:8080/author/countDev')
  .map(resp=>resp.json());
}


ModifyAuthor(author:Author,id){
  return this.http.put('http://localhost:8080/author/updateAdmin?id='+id,author);

}


GetAuthorByUserName(username:String) : Observable<Author[]> {
  // ...using get request


  let response =this.http.get("http://localhost:8080/author/getUserbyusername?username="+username)
     // ...and calling .json() on the response to return data
     .map((response) => response.json());
 return response;
}




GetUsernames() : Observable<String[]> {
  // ...using get request
  let response = this.http.get("http://localhost:8080/author/getUsersByUsername")
     // ...and calling .json() on the response to return data
     .map((response) => response.json());
 return response;
}


}
