import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private http:HttpClient) { }
  getAllTeam()
  {return this.http.get("http://localhost:8080/getAllTeam");}
}
