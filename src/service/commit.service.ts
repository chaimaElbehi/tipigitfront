import { commit } from './../model/model.commit';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommitService {

  constructor(public http:Http) { }
  getCommitByusername(username): Observable<commit[]>
  {
    let response =this.http.get('http://localhost:8080/commit/getAllCommitsInfoByUser?authorName='+username)
    // ...and calling .json() on the response to return data
    .map((response) => response.json());
return response;
  }
}
