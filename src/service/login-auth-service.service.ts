import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { text } from '@angular/core/src/render3/instructions';
import { RouterLink } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginAuthServiceService {
  data: any;
  sess:any;
  role:any;
  roleSess:string;
  options = { responseType: 'text' as 'text'};


  constructor(private http: HttpClient) { }

 public login(nameUser: String, passwordUser: String): Observable<boolean> {
return this.http.get<boolean>('http://localhost:8080/author/loginAuthReq?username=' + nameUser + '&password=' + passwordUser);
}
getRoleByUsername(username)
{
  return this.http.get<any>('http://localhost:8080/author/userRole?username='+username
  );

}


getRole(username)
{
  this.getRoleByUsername(username).subscribe(data=>{
    this.role=data;
    sessionStorage.setItem("role", this.role.role);

  })
this.roleSess=sessionStorage.getItem("role");
}


getAuthorsName()  {
  // ...using get request

  let response =this.http.get("http://localhost:8080/author/authorsName")
     // ...and calling .json() on the response to return data
     .map((response) => response);
 return response;
}
authenticate(username, password) {

  this.login(username, password).subscribe(data => { this.data = data });
  if (this.data) {

  sessionStorage.setItem('username', username);
  sessionStorage.setItem('role',this.role);
  return true;
  } else {
  return false;
  }
  }
isAdmin()
{
  this.roleSess=sessionStorage.getItem('role');
  //console.log(this.roleSess)
  //console.log((this.roleSess==='Admin'));
  return (this.roleSess==='Admin')
}

//loginVerification function
  isUserLoggedIn() {
    this.sess = sessionStorage.getItem('username')
    //console.log(!(this.sess === null))
    return !(this.sess === null)
    }



//logout function
    logOut() {
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('role');
    }
    //getters ans setters
    getroleSess()
    {
      return this.getroleSess;
    }
    setroleSess(role)
    {
      this.roleSess=role;
    }
  }



