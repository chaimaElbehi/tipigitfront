import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class projectService {

    constructor(private http:HttpClient) {}

    // Uses http.get() to load data from a single API endpoint

    getAllProjects(){
      return this.http.get('http://localhost:8080/allProject');

  }
  getProjectByAuthorusername(username)
  {return this.http.get("http://localhost:8080/AuthorByProject/projectByAuthorusername?username="+username);}


  setbacklog(sheetID,projectName)
  {
    return this.http.get('http://localhost:8080/googlesheets?sheetID=' + sheetID + '&projectName=' + projectName);
  }
  countProjects()
  {
    return this.http.get('http://localhost:8080/countProject');
  }


}
