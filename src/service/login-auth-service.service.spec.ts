import { TestBed, inject } from '@angular/core/testing';

import { LoginAuthServiceService } from './login-auth-service.service';

describe('LoginAuthServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginAuthServiceService]
    });
  });

  it('should be created', inject([LoginAuthServiceService], (service: LoginAuthServiceService) => {
    expect(service).toBeTruthy();
  }));
});
