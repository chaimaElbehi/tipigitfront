import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { notificationTask } from 'src/model/notificationTask';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskNotificationService {

  constructor( private http:HttpClient) { }

  countNotifs(usename)
  {
    return this.http.get('http://localhost:8080/notificationCountUser?username='+usename);
  }

  getNotifs(username): Observable<notificationTask[]>
  {
    return this.http.get<notificationTask[]>("http://localhost:8080/getNotification?username="+username);

}

}

