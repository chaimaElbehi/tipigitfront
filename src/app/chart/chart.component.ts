import { Component, OnInit } from '@angular/core';
import {Chart} from 'chart.js';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  LineChart=[];
  constructor() { }

  ngOnInit() {


    this.LineChart = new Chart('lineChart', {
      type: 'line',
    data: {
     labels: ["Jan", "Feb", "March", "April", "May", "June","July"],
     datasets: [{
         label: 'Chaima Elbehi Essefi',
         data: [0,2 , -2, 5, 9, 10,15],
         fill:false,
         lineTension:0.2,
         borderColor:"deeppink",
         borderWidth: 2
     },
     {
      label: 'Mohamed Slama',
      data: [0,-2,4,1,9,10,6],
      fill:false,
      lineTension:0.2,
      borderColor:"strongblue",
      borderWidth: 3
  }

    ]
    },
    options: {
     title:{
         text:"Performance Chart",
         display:true
     },
     scales: {
         yAxes: [{
             ticks: {
                 beginAtZero:true
             }
         }]
     }
    }
    });
  }

}
