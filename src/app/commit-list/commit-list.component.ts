import { commit } from './../../model/model.commit';
import { CommitService } from './../../service/commit.service';
import { Http } from '@angular/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-commit-list',
  templateUrl: './commit-list.component.html',
  styleUrls: ['./commit-list.component.css']
})
export class CommitListComponent implements OnInit {
  commitList:commit[];
  constructor(private http:Http,private commitService:CommitService) {

  }

  ngOnInit() {
    this.getAllCommit(sessionStorage.getItem('username'));
  }

getAllCommit(username)
{



  console.log(this.commitService.getCommitByusername(username)
  .subscribe(data=>{
    // console.log(data);
     this.commitList=data;
    // console.log(data);
    console.log(this.commitList);

   }))
}
}
