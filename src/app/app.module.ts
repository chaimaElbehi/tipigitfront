import { sprintService } from './../service/sprints.service';
import { projectService } from './../service/project.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
    import { HttpClientModule, HttpClient } from '@angular/common/http';
    import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AuthorsComponent } from './authors/authors.component';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { HttpModule } from '@angular/http';
import { AuthorService } from 'src/service/authors.service';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './home/header/header.component';
import { FooterComponent } from './home/footer/footer.component';
import { LayoutComponent } from './home/layout/layout.component';
import { HomeComponent } from './home/home.component';
import { TestComponent } from './test/test.component';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TaskService } from 'src/service/task.service';
import { CommitsListComponent } from './home/layout/commits-list/commits-list.component';
import { ReportsComponent } from './reports/reports.component';
import { PojectUserComponent } from './poject-user/poject-user.component';
import { WebSocketService } from 'src/service/web-socket-service.service';
import { NotificationComponent } from './notification/notification.component';
import { ReportsService } from 'src/service/reports.service';
import { ChartComponent } from './chart/chart.component';
import { CommitListComponent } from './commit-list/commit-list.component';




const appRoute: Routes = [


  {
    path: 'about',component: AboutComponent},
    {path:'reports',component:ReportsComponent},
    {path: 'login' ,component: LoginComponent},
  {path:'authors',component:AuthorsComponent},
  {path: '', component: LoginComponent},
  {path: 'home' ,component: HomeComponent,
  children: [
    {path: 'authors', component:AuthorsComponent},
    {path: 'projects', component:LayoutComponent},
    {path:'about',component:AboutComponent},
    {path:'',component:AboutComponent},
    {path:'test',component:TestComponent},
    {path:'authProject',component:PojectUserComponent},
    {path:'report',component:ReportsComponent},
    {path:'notification',component:NotificationComponent},
    {path:'commmitList',component:CommitListComponent},
    {path:'chart',component:ChartComponent}
  ]
  },
  {path:'commits',component:CommitsListComponent},
  {path:'authProject123',component:PojectUserComponent},
  {path:'notification',component:NotificationComponent},






];
@NgModule({
  declarations: [
    AppComponent,
    AuthorsComponent,
    AboutComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    LayoutComponent,
    HomeComponent,
    TestComponent,
    CommitsListComponent,
    ReportsComponent,
    PojectUserComponent,
    NotificationComponent,
    ChartComponent,
    CommitListComponent,


  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoute),
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,

    RouterModule.forChild(appRoute),
    HttpModule,

    HttpClientModule,
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn'
  })
  ],
  providers: [AuthorService, projectService,sprintService,TaskService,WebSocketService,ReportsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
