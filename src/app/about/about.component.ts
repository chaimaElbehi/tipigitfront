import { AuthorService } from "./../../service/authors.service";
import { projectService } from "./../../service/project.service";
import { Component, OnInit } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";
import { Author } from "src/model/model.author";

@Component({
  selector: "app-about",
  templateUrl: "./about.component.html",
  styleUrls: ["./about.component.css"]
})
export class AboutComponent implements OnInit {
  NumberProject: any;
  NumberAuthors: any;
  auth: Author=new Author();
  userSess = sessionStorage.getItem("username");
  constructor(
    private http: Http,
    public projectService: projectService,
    public authorService: AuthorService
  ) {}

  ngOnInit() {
    this.projectService.countProjects().subscribe(
      data => {
        this.NumberProject = data;
      },
      err => {
        console.log(err);
      }
    );

    this.authorService.CountAuthors().subscribe(
      data => {
        this.NumberAuthors = data;
      },
      err => {
        console.log(err);
      }
    );
    this.getUserInformations(this.userSess);
  }

  getUserInformations(username) {
    this.authorService.getAuthorInfoByusename(username).subscribe(
      data => {
        this.auth = data;

        console.log(data['email']);
        console.log(this.auth);
      },
      err => {
        console.log(err);
      }
    );
  }
}
