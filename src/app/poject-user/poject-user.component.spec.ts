import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PojectUserComponent } from './poject-user.component';

describe('PojectUserComponent', () => {
  let component: PojectUserComponent;
  let fixture: ComponentFixture<PojectUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PojectUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PojectUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
