import { Component, OnInit } from '@angular/core';
import { WebSocketService } from 'src/service/web-socket-service.service';
import { notification } from 'src/model/notification';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  public notifications :notification=new notification() ;
width:any;
  public notificationsTotal:any;
  public notificationListKeys;
  notif:any[];
  constructor(private webSocketService: WebSocketService) {

  // Open connection with server socket
      let stompClient = this.webSocketService.connect();
      stompClient.connect({}, frame => {

    // Subscribe to notification topic
          stompClient.subscribe('/topic/notification', notifications => {

      // Update notifications attribute with the recent messsage sent from the server
              //this.notifications.counter = JSON.parse(notifications.body).count;
              //this.notifications.name = JSON.parse(notifications.body).username;
              //this.notifications.List=JSON.parse(notifications.body).taskIdStarted;
              //this.notificationListKeys = Object.keys(this.notifications.List);
              //console.log(this.notifications.List);
              this.notificationsTotal=JSON.parse(notifications.body);
              console.log(JSON.parse(notifications.body));


          })
      });
  }

  ngOnInit() {
    this.width=40;
  }

}
