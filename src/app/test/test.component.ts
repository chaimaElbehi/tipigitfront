import { TaskService } from 'src/service/task.service';
import { Component,ViewEncapsulation , OnInit, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { AuthorService } from 'src/service/authors.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { task } from 'src/model/model.task';



declare var $;


@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',

  encapsulation: ViewEncapsulation.None
})
export class TestComponent  {
TaskInprogress:any;
TaskTodo:any;
TaskDone:any;

  closeResult: string;
user=sessionStorage.getItem("username");
  constructor(private modalService: NgbModal,public taskServcie:TaskService ){


  }
  ngOnInit() {
    this.showInprogressTasks();
    this.showDoneTasks();
    this.showToDoTasks();
  }

  showDoneTasks()
  {
    this.taskServcie.getAllDoneTasks(this.user)
    .subscribe(data=>{
      this.TaskDone=data;
console.log(this.TaskDone.length)
    },err=>{
      console.log(err);
    })
  }
  showToDoTasks(){
this.taskServcie.getAllToDoTasks(this.user)
.subscribe(data=>{
  this.TaskTodo=data;

},err=>{
  console.log(err);
})

  }
  showInprogressTasks()
  {console.log(this.user);
    this.taskServcie.getAllInprogressTaskByAuthorName(this.user)

    .subscribe(data=>{
      this.TaskInprogress=data;

    },err=>{
      console.log(err);
    })
  }

  hello()
  {console.log("rrrrr");}
  openVerticallyCentered(content) {
this.modalService.open(content);
  }
  open(content){
    this.modalService.open(content);
  }
}
