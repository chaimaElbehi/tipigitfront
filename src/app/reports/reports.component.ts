import { TaskService } from 'src/service/task.service';
import { Component, OnInit } from '@angular/core';
import { ReportsService } from 'src/service/reports.service';
import{Chart} from 'chart.js';
import { projectService } from 'src/service/project.service';
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  chart = []; // This will hold our chart info

  constructor(private reports:ReportsService,public projectService: projectService,public taskService:TaskService) { }
  title = 'dashboard';
  numberNotStarted:any;
  numberStratedNotFinsh:any;
  numberStartFinish:any;
  nameProject;
  num1:Number=0;
  Notshow=true;

  chart2 = [];
  pie: any;
  doughnut: any;
  line:any;
  ListProject:any;



  data1 = [];
  ngOnInit() {

    this.projectService.getAllProjects()
    .subscribe(
      data => {
        this.ListProject = data;
      }, err => {
        console.log(err);
      })





    /*socket.on('data1', (res) => {
      this.updateChartData(this.chart, res, 0);
      this.updateChartData(this.doughnut,res.slice(0,5), 0);
    })

    socket.on('data2', (res) => {
      this.updateChartData(this.chart, res, 1);
    })*/

    this.chart = new Chart('bar', {
      type: 'bar',
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Combo Bar and line Chart'
        },
      },
      data: {
        labels: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'],
        datasets: [
          {
            type: 'bar',
            label: 'My First dataset',
            data: [243, 156, 365, 30, 156, 265, 356, 543],
            backgroundColor: 'rgba(255,0,255,0.4)',
            borderColor: 'rgba(255,0,255,0.4)',
            fill: false,
          },

          {
            type: 'bar',
            label: 'My Second dataset',
            data: [243, 156, 365, 30, 156, 265, 356, 543].reverse(),
            backgroundColor: 'rgba(0,0,255,0.4)',
            borderColor: 'rgba(0,0,255,0.4)',
            fill: false,
          }
        ]
      }
    });

    let options = {
      // aspectRatio: 1,
      // legend: false,
      tooltips: false,

      elements: {
        point: {
          borderWidth: function (context) {
            return Math.min(Math.max(1, context.datasetIndex + 1), 8);
          },
          hoverBackgroundColor: 'transparent',
          hoverBorderColor: function (context) {
            return "red";
          },
          hoverBorderWidth: function (context) {
            var value = context.dataset.data[context.dataIndex];
            return Math.round(8 * value.v / 1000);
          },
          radius: function (context) {
            var value = context.dataset.data[context.dataIndex];
            var size = context.chart.width;
            var base = Math.abs(value.v) / 1000;
            return (size / 24) * base;
          }
        }
      }
    };

this.line=new Chart('line',
{
     type: 'line',
     label: 'Dataset 2',
     backgroundColor: 'rgba(0,0,255,0.4)',
     borderColor: 'rgba(0,0,255,0.4)',
     data: [
       443, 256, 165, 100, 56, 65, 35, 543
     ],
     fill: true,

})





    this.pie = new Chart('pie',{
      type: 'pie',
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Pie Chart'
        },legend: {
					position: 'top',
				},animation: {
					animateScale: true,
					animateRotate: true
				}
      },
      data: {
				datasets: [{
					data: [45,10,5,25,15].reverse(),
					backgroundColor: ["red","orange","yellow","green","blue"],
					label: 'Dataset 1'
				}],
				labels: ['Red','Orange','Yellow','Green','Blue']
			}
    })

  }
  downloadCanvas(event) {
    // get the `<a>` element from click event
    var anchor = event.target;
    // get the canvas, I'm getting it by tag name, you can do by id
    // and set the href of the anchor to the canvas dataUrl
    anchor.href = document.getElementsByTagName('canvas')[0].toDataURL();
    // set the anchors 'download' attibute (name of the file to be downloaded)
    anchor.download = this.nameProject+"_charts"+".tiff";
}


  addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    chart.update();
}

updateChartData(chart, data, dataSetIndex){
  chart.data.datasets[dataSetIndex].data = data;
  chart.update();
}

showGraphe(num1,num2,num3){

//if((num1===0)&&(num2===0)&&(num3===0))
//this.Notshow=false;
//else
//this.Notshow=true;

//this.updateChartData(this.chart, [num1,num2,num3], 0);
//this.updateChartData(this.doughnut,[num1,num2,num3],0)
  this.doughnut =  new Chart('doughnut',{
    type: 'doughnut',
    options: {
      responsive: true,
      title: {
        display: true,
        text: 'Task Progression Chart for ' + this.nameProject
      },legend: {
        position: 'top',
      },animation: {
        animateScale: true,
        animateRotate: true
      }
    },

    data: {
      datasets: [{
        data: [num1,num2,num3],
        backgroundColor: ["red","orange","green"],
        label: 'Dataset 1'
      }],
      labels: ['Not started Tasks','In progress tasks','Done tasks']
    }
  })
}


fetchData(nameProj){
  console.log(nameProj);
  var canvas = <HTMLCanvasElement> document.getElementById("doughnut");
  var ctx: CanvasRenderingContext2D = canvas.getContext("2d");
  ctx.restore();




  this.nameProject=nameProj;
  console.log(this.nameProject);
  var _this = this;
  var _res;
  this.taskService.CountprogressionTasks(nameProj)
  .subscribe(res => {
     console.log('res   ' + res);
      _res = res;


    // this.taskService.setcountTodoTask(this.number);

  },()=>{}, () => {
    _this.numberNotStarted=_res['NotStartedNotFinished'];
    _this.numberStratedNotFinsh=_res['StartedNotFinished'];
    _this.numberStartFinish= _res['finishedTasks'];




    console.log(_this.numberNotStarted);
    console.log(_this.numberStratedNotFinsh);
    console.log(_this.numberStartFinish);
    this.showGraphe(_this.numberNotStarted,_this.numberStratedNotFinsh,_this.numberStartFinish);
  })
}




}



