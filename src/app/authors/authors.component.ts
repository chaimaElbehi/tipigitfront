import { TeamService } from './../../service/team.service';
import { Component, OnInit, Input } from '@angular/core';
import{ Http} from "@angular/http";
import 'rxjs/add/operator/map' ;
import { AuthorService } from 'src/service/authors.service';
import Swal from 'sweetalert2';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Author } from 'src/model/model.author';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { loginAsyncValidator } from './loginAsyncValidator';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  hidden:boolean;
  public form: FormGroup;
  public form01:FormGroup;
  public formGlobal:FormGroup;
  TeamList:any;
  idAuth:any;
pageAuthors:any;
author :Author=new Author();
keyWord:string="";
currentPage:number=1;
size:number=2;
pages:Array<number>;
  constructor(private http:Http,public teamService:TeamService,public authorService: AuthorService,public builder: FormBuilder) {
/////********** */*for form required element validation*////



this.formGlobal = this.builder.group({
  login: new FormControl(
    null, Validators.required, loginAsyncValidator(this.authorService)),
    pass:new FormControl(
      Validators.minLength(6),Validators.required
    )

});




/////********** */*end form required element validation*////


   }



  ngOnInit() {
this.doSearch();

  }


  getTeam()
  {
    this.teamService.getAllTeam();
console.log(this.teamService.getAllTeam().subscribe(data=>{
 // console.log(data);
  this.TeamList=data;
  console.log(this.TeamList);
}))

  }
  ModifyAuthor(author1:Author){
console.log(author1);
console.log(this.idAuth);


this.authorService.ModifyAuthor(author1,this.idAuth)
  .subscribe(data=>{
    this.doSearch();
  });


Swal.fire(
  'Updated!',
  'Your Author has been deleted.',
  'success'
)

  }

  hello1(id,name,email,role)
  { console.log(id);
  console.log(name);
console.log(email);
this.idAuth=id;
this.author.name=name;
this.author.email=email;
this.author.role=role;}
  gotoPage(i:number)
  {
    this.currentPage=i;
    this.doSearch(); }
    delete(id)
    {
      console.log(id);

      console.log("button success clicked");
      this.authorService.deleteAuthor(id).subscribe(data=>{
        this.doSearch();
      });

      this.pageAuthors= this.pageAuthors.filter(h => h.id !== id);


   }

  doSearch(){
    this.authorService.getAuthors(this.keyWord,this.currentPage,this.size)
    .subscribe(data=>{
      this.pageAuthors=data;
      this.pages=new Array(data.totalPages);
    },err=>{
      console.log(err);
    })
  }
  search()
  {
this.doSearch();
  }


  showModal(id)
  {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#8bc34a',
      cancelButtonColor: '#d33',
      cancelButtonText: '<strong>Cancel</strong>',
      confirmButtonText: '&nbsp;<strong>Yes</strong>'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your Author has been deleted.',
          'success'
        )


        this.delete(id);
      }
    })
}
}
