import { AuthorService } from './../../service/authors.service';
import { Author } from './../../model/model.author';
import { FormControl } from '@angular/forms';
import { timer } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';


export const loginAsyncValidator = (authorService:AuthorService, time: number = 100) => {
return (input: FormControl) => {
return timer(time).pipe(
switchMap(() => authorService.checkLogin(input.value)),
map(res => {
return res.isLoginAvailable ? null : { loginExist: true };
})
);
};
};
