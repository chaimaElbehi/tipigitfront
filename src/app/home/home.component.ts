import { Author } from './../../model/model.author';
import { AuthorService } from './../../service/authors.service';
import { LoginAuthServiceService } from './../../service/login-auth-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
auth:Author;
userSess:string;
  constructor(public loginAuthService:LoginAuthServiceService,public authorService:AuthorService) { }

  ngOnInit() {
    this.userSess=sessionStorage.getItem("username");
    console.log(sessionStorage.getItem("username"));
    console.log(sessionStorage.getItem('role'));

  }


}
