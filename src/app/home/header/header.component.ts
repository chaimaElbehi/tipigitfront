import { TaskNotificationService } from './../../../service/task-notification.service';
import { LoginAuthServiceService } from './../../../service/login-auth-service.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { notificationTask } from 'src/model/notificationTask';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  Username:any;
  nbrNotif;
  ListNotifs:notificationTask[]=new Array();

  constructor( public TaskNotif:TaskNotificationService,public loginauth:LoginAuthServiceService,private router:Router) {

    this.Username=sessionStorage.getItem('username');

   }
  logout(){
    console.log('logout field')
    this.loginauth.logOut();
    this.router.navigate(['login']);


  }
  ngOnInit() {
    this.TaskNotif.countNotifs(this.Username).subscribe(

      res => {
        //  console.log(res)
           this.nbrNotif=res;


  })
  this.TaskNotif.getNotifs(this.Username).subscribe(
    res => {
      //  console.log(res)
         this.ListNotifs=res;

       console.log(this.ListNotifs);

    })

}}
