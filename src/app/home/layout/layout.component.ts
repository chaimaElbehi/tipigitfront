import { AuthorService } from './../../../service/authors.service';
import { TaskService } from './../../../service/task.service';
import { sprintService } from './../../../service/sprints.service';
import { projectService } from './../../../service/project.service';
import { Component, OnInit, Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http} from '@angular/http';
import 'rxjs/add/operator/map' ;
import Swal from 'sweetalert2';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})

export class LayoutComponent implements OnInit {
  ListSprints: any;
  sheetsId:any;
ListProject: any;
nameProject:String;
ListTask:any;
authName:String;
AuthorsName: String[];
showSpinner:boolean;


constructor(private http: Http,public authorService:AuthorService, public projectService: projectService, public sprintService: sprintService, public taskService: TaskService ) { }
  ngOnInit() {
    this.showSpinner = false;
    this.projectService.getAllProjects()
    .subscribe(
      data => {
        this.ListProject = data;
      }, err => {
        console.log(err);
      })

     this.getAllSprints();




}
//changing the author name for a task
saveauthName(idTask)
{console.log(this.authName);
this.taskService.SetAuthorTaskName(idTask,this.authName).subscribe();
//showing sweetAlert modification
Swal.fire(
  'Updated!',
  'the owner of the task has been updated',
  'success'
)
}
/*************** */
//adding backlog to a project function
addBacklog(){
  this.showSpinner = true;
  console.log(this.nameProject);
  console.log(this.sheetsId);
  this.projectService.setbacklog(this.sheetsId,this.nameProject).subscribe(
    (err) => console.error(err),
    // The 3rd callback handles the "complete" event.
    () => {console.log("observable complete")
    this.showSpinner = false;
if(this.sheetsId)
   { Swal.fire(
      'added!',
      'backlog has been added',
      'success'
    )
  }

else
{
  Swal.fire(
    'fail!',
    'no data to add !!!',
    'warning'
  )
}}
  );
  this.getAllSprints();

}
recuperateProjectName(name:String)
{
  this.nameProject=name;
}
resuperateidSprint(id)
{
  console.log(id);}
GetAuthorsName()
{
  this.authorService.getAuthorsName().subscribe(
    AuthorsName=>{
      this.AuthorsName=AuthorsName;

    }
  )
  console.log("author name :p : ");
  console.log(this.AuthorsName.toString);
}


  GetListTask(idSprint)
{console.log(idSprint);
  this.taskService.getTasksBySprintId(idSprint).subscribe(
data=>{
  this.ListTask=data;
},err =>
{
  console.log(err);
}
  )
this.GetAuthorsName();}
SearchSprint(name:String) {
  console.log("name is ...........",name);
  this.sprintService.getAllSprints(name)
  .subscribe(
    data => {
      this.ListSprints=data;
      console.log(data);
    },err =>
    {
      console.log(err);
    }
  )
    }

    selectChangeHandler (event: any) {

      this.authName = event.target.value;
    }

getAllSprints(){
  this.sprintService.getAllSprints(name)
  .subscribe(
    data => {
      this.ListSprints=data;
    },err =>
    {
      console.log(err);
    }
  )
      }
}
