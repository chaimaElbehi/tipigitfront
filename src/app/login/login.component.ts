import { TaskService } from './../../service/task.service';
import { LoginAuthServiceService } from './../../service/login-auth-service.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalidLogin:any;

  username:String;
  password:String;
  constructor(private loginAuthService: LoginAuthServiceService,private router:Router  ) {


  }
  checkLogin(username, password) {
    var _this = this;
    console.log(this.loginAuthService.authenticate(username, password));
    _this.loginAuthService.getRole(username);
    if ((_this.loginAuthService.authenticate(username, password))) {
    this.router.navigate(['home']);
    _this.invalidLogin = false;
    } else
    _this.invalidLogin = true;
    }

  ngOnInit() {
  }

}
